python-moderngl (5.12.0-1) unstable; urgency=medium

  * Update d/watch to latest GitHub recipe
  * New upstream version 5.12.0
  * Add test depend on python3-opengl
  * Bump Standards-Version to 4.7.0
  * Build documentation with default Python
  * Drop unused license paragraphs from d/copyright

 -- Timo Röhling <roehling@debian.org>  Mon, 04 Nov 2024 09:50:40 +0100

python-moderngl (5.11.1-1) unstable; urgency=medium

  * New upstream version 5.11.1

 -- Timo Röhling <roehling@debian.org>  Wed, 28 Aug 2024 15:51:58 +0200

python-moderngl (5.10.0-1) unstable; urgency=medium

  * New upstream version 5.10.0

 -- Timo Röhling <roehling@debian.org>  Tue, 23 Jan 2024 15:19:03 +0100

python-moderngl (5.8.2-3) unstable; urgency=medium

  * Drop obsolete workaround for PYBUILD_TEST_CUSTOM

 -- Timo Röhling <roehling@debian.org>  Sun, 15 Oct 2023 15:42:16 +0200

python-moderngl (5.8.2-2) unstable; urgency=medium

  * Drop patch to not use Furo theme for documentation

 -- Timo Röhling <roehling@debian.org>  Fri, 28 Jul 2023 23:54:23 +0200

python-moderngl (5.8.2-1) unstable; urgency=medium

  * Make package cross-buildable
  * Bump Standards-Version to 4.6.2
  * New upstream version 5.8.2

 -- Timo Röhling <roehling@debian.org>  Mon, 12 Jun 2023 21:49:59 +0200

python-moderngl (5.7.4-1) unstable; urgency=medium

  * New upstream version 5.7.4
  * Do not ship src folder in binary package

 -- Timo Röhling <roehling@debian.org>  Fri, 16 Dec 2022 13:49:23 +0100

python-moderngl (5.7.3-2) unstable; urgency=medium

  * Use autopkgtest-pkg-pybuild

 -- Timo Röhling <roehling@debian.org>  Sat, 10 Dec 2022 23:09:25 +0100

python-moderngl (5.7.3-1) unstable; urgency=medium

  * New upstream version 5.7.3

 -- Timo Röhling <roehling@debian.org>  Wed, 30 Nov 2022 16:03:53 +0100

python-moderngl (5.7.2-1) unstable; urgency=medium

  * New upstream version 5.7.2

 -- Timo Röhling <roehling@debian.org>  Thu, 10 Nov 2022 13:36:14 +0100

python-moderngl (5.7.1-1) unstable; urgency=medium

  * New upstream version 5.7.1

 -- Timo Röhling <roehling@debian.org>  Wed, 02 Nov 2022 18:58:27 +0100

python-moderngl (5.7.0-1) unstable; urgency=medium

  * New upstream version 5.7.0
  * Refresh patches
    - Drop 0001-Use-sphinx.ext.napoleon-instead-of-sphinxcontrib.nap.patch
      (merged upstream)
    - Add 0001-Use-sphinx-rtd-theme.patch
  * Switch d/watch to api.github.com
  * Bump Standards-Version to 4.6.1
  * Add hardening flags to build

 -- Timo Röhling <roehling@debian.org>  Fri, 28 Oct 2022 13:30:22 +0200

python-moderngl (5.6.4-2) unstable; urgency=medium

  * Source only upload.

 -- Timo Röhling <roehling@debian.org>  Mon, 06 Jun 2022 19:49:41 +0200

python-moderngl (5.6.4-1) unstable; urgency=medium

  * Initial release. (Closes: #1009238)

 -- Timo Röhling <roehling@debian.org>  Sat, 09 Apr 2022 18:57:04 +0200
